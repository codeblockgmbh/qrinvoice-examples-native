Sub CreateQrInvoice()

	ChDir ActiveWorkbook.Path
	QrInvoiceBaseDir = ActiveWorkbook.Path & "\QrInvoice"
	QrInvoiceNativePath = QrInvoiceBaseDir & "\qrinvoice_ppr.exe --inputfile " & ActiveWorkbook.Path & "\inputdata.json --outputfile " & ActiveWorkbook.Path & "\qrinvoice_a4.pdf --logfile " & ActiveWorkbook.Path & "\qrinvoice.log --fontpath " & QrInvoiceBaseDir

	RetVal = Shell(QrInvoiceNativePath, vbNormalFocus)

	ActiveWorkbook.FollowHyperlink ActiveWorkbook.Path & "\qrinvoice_a4.pdf"

End Sub

﻿using System;
using System.Text;
using System.IO;

namespace c_sharp_simple_dll
{
    class Program
    {
        static void Main(string[] args)
        {
            QrInvoice.SetFontPath(QrInvoice.QR_INVOICE_DIR);
            Console.WriteLine("-------------");
            CreateSwissQrCodePng();
            Console.WriteLine("-------------");
            CreatePaymentPartReceiptPng();
            Console.WriteLine("-------------");
            CreatePaymentPartReceiptPdf();
            Console.WriteLine("-------------");
        }

        private const string INPUT_DATA_JSON = "{" +
            "  \"creditorInformation\":{" +
            "    \"iban\":\"CH4431999123000889012\"," +
            "    \"creditor\":{" +
            "      \"addressType\":\"STRUCTURED\"," +
            "      \"name\":\"Robert Schneider AG\"," +
            "      \"streetName\":\"Rue du Lac\"," +
            "      \"houseNumber\":\"1268\"," +
            "      \"postalCode\":\"2501\"," +
            "      \"city\":\"Biel\"," +
            "      \"country\":\"CH\"" +
            "    }" +
            "  }," +
            "  \"paymentAmountInformation\":{" +
            "    \"currency\":\"CHF\"," +
            "    \"amount\":1949.75" +
            "  }," +
            "  \"ultimateDebtor\":{" +
            "    \"addressType\":\"STRUCTURED\"," +
            "    \"name\":\"Pia-Maria Rutschmann-Schnyder\"," +
            "    \"streetName\":\"Grosse Marktgasse\"," +
            "    \"houseNumber\":\"28\"," +
            "    \"postalCode\":\"9400\"," +
            "    \"city\":\"Rorschach\"," +
            "    \"country\":\"CH\"" +
            "  }," +
            "  \"paymentReference\":{" +
            "    \"referenceType\":\"QRR\"," +
            "    \"reference\":\"210000000003139471430009017\"," +
            "    \"additionalInformation\":{" +
            "      \"unstructuredMessage\":\"Instruction of 03.04.2019\"" +
            "    }" +
            "  }," +
            "  \"alternativeSchemes\":{" +
            "    \"alternativeSchemeParameters\":[" +
            "      \"Name AV1: UV;UltraPay005;12345\"," +
            "      \"Name AV2: XY;XYService;54321\"" +
            "    ]" +
            "  }" +
            "}";


        private static void CreateSwissQrCodePng()
        {
            String jsonBase64 = Base64Encode(INPUT_DATA_JSON);

            Int32 OutputBufferLength = 25000;
            StringBuilder OutputBuffer = new StringBuilder((int)OutputBufferLength);
            Int32 MessageBufferLength = 5000;
            StringBuilder MessageBuffer = new StringBuilder((int)MessageBufferLength);

            int retVal = QrInvoice.CreateSwissQrCodePngJsonBase64(500, jsonBase64, OutputBuffer, ref OutputBufferLength, MessageBuffer, ref MessageBufferLength);

            if (retVal == 0)
            {
                string base64 = OutputBuffer.ToString();
                File.WriteAllBytes("swissqrcode.png", Base64Decode(base64));
                System.Console.WriteLine("swissqrcode.png written");
            }
            else
            {
                System.Console.WriteLine("CreateSwissQrCodePng error: " + retVal);
                string base64 = MessageBuffer.ToString();
                System.Console.WriteLine(System.Text.Encoding.UTF8.GetString(Base64Decode(base64)));
            }
        }

        private static void CreatePaymentPartReceiptPng()
        {
            String jsonBase64 = Base64Encode(INPUT_DATA_JSON);

            Int32 OutputBufferLength = 450000;
            StringBuilder OutputBuffer = new StringBuilder((int)OutputBufferLength);
            Int32 MessageBufferLength = 5000;
            StringBuilder MessageBuffer = new StringBuilder((int)MessageBufferLength);

            int retVal = QrInvoice.CreatePprPngJsonBase64("DIN_LANG", "LiberationSans", "GERMAN", 300, true, true, false, jsonBase64, OutputBuffer, ref OutputBufferLength, MessageBuffer, ref MessageBufferLength);

            if (retVal == 0)
            {
                string base64 = OutputBuffer.ToString();
                File.WriteAllBytes("paymentpartreceipt.png", Base64Decode(base64));
                System.Console.WriteLine("paymentpartreceipt.png written");
            }
            else
            {
                System.Console.WriteLine("CreatePaymentPartReceiptPng error: " + retVal);
                string base64 = MessageBuffer.ToString();
                System.Console.WriteLine(System.Text.Encoding.UTF8.GetString(Base64Decode(base64)));
            }
        }

        private static void CreatePaymentPartReceiptPdf()
        {
            String jsonBase64 = Base64Encode(INPUT_DATA_JSON);

            Int32 OutputBufferLength = 100000;
            StringBuilder OutputBuffer = new StringBuilder((int)OutputBufferLength);
            Int32 MessageBufferLength = 5000;
            StringBuilder MessageBuffer = new StringBuilder((int)MessageBufferLength);

            int retVal = QrInvoice.CreatePprPdfJsonBase64("A4", "LiberationSans", "FRENCH", true, true, true, jsonBase64, OutputBuffer, ref OutputBufferLength, MessageBuffer, ref MessageBufferLength);

            if (retVal == 0)
            {
                string base64 = OutputBuffer.ToString();
                File.WriteAllBytes("paymentpartreceipt.pdf", Base64Decode(base64));
                System.Console.WriteLine("paymentpartreceipt.pdf written");
            }
            else
            {
                System.Console.WriteLine("CreatePaymentPartReceiptPdf error: " + retVal);
                string base64 = MessageBuffer.ToString();
                System.Console.WriteLine(System.Text.Encoding.UTF8.GetString(Base64Decode(base64)));
            }
        }


        public static string Base64Encode(string data)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(data);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static byte[] Base64Decode(String base64)
        {
            return System.Convert.FromBase64String(base64);
        }
    }
}

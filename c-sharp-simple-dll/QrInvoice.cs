﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace c_sharp_simple_dll
{
    class QrInvoice
    {
        public const string QR_INVOICE_DIR = @"C:\dev\git\qrinvoice-examples-native\c-sharp-simple-dll\QrInvoice\bin\x64\";
        const string QR_INVOICE_DLL = QR_INVOICE_DIR + "qrinvoice_simple.dll";

        [DllImport(QR_INVOICE_DLL, EntryPoint = "SetFontPath", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetFontPath(String FontPath);

        [DllImport(QR_INVOICE_DLL, EntryPoint = "CreateSwissQrCodePngJsonBase64", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreateSwissQrCodePngJsonBase64(int DesiredQrCodeSize, String JsonInputBase64,
            StringBuilder OutputBuffer, ref Int32 OutputBufferLength,
            StringBuilder MessageBuffer, ref Int32 MessageBufferLength
        );

        [DllImport(QR_INVOICE_DLL, EntryPoint = "CreatePprPngJsonBase64", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreatePprPngJsonBase64(String PageSize, String FontFamily, String Language, int Resolution,
            Boolean BoundaryLines, Boolean BoundaryLineScissors, Boolean BoundaryLineSeparationText, String JsonInputBase64,
            StringBuilder OutputBuffer, ref Int32 OutputBufferLength,
            StringBuilder MessageBuffer, ref Int32 MessageBufferLength
        );

        [DllImport(QR_INVOICE_DLL, EntryPoint = "CreatePprPdfJsonBase64", CharSet = CharSet.Ansi, SetLastError = true, CallingConvention = CallingConvention.Cdecl)]
        public static extern int CreatePprPdfJsonBase64(String PageSize, String FontFamily, String Language,
            Boolean BoundaryLines, Boolean BoundaryLineScissors, Boolean BoundaryLineSeparationText, String JsonInputBase64,
            StringBuilder OutputBuffer, ref Int32 OutputBufferLength,
            StringBuilder MessageBuffer, ref Int32 MessageBufferLength
        );

    }
}

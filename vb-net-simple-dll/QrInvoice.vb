﻿Imports System.Text
Imports Microsoft.VisualBasic

Imports System.Runtime.InteropServices

' This class contains the DLL Binding
Public Class QrInvoice
    Public Const QR_INVOICE_DIR = "C:\\dev\\git\\qrinvoice-examples-native\\vb-net-simple-dll\\QrInvoice\bin\\x64\\"
    Const DLL_PATH = QR_INVOICE_DIR & "qrinvoice_simple.dll"

    <DllImportAttribute(DLL_PATH, EntryPoint:="SetFontPath",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Sub SetFontPath(ByVal FontPath As String)
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Sub

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreateSwissQrCodePngCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateQrCodePngCsvBase64(ByVal DesiredQrCodeSize As Integer, ByVal CsvInput As String,
                                        ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                        ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreateSwissQrCodePngJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateQrCodePngJsonBase64(ByVal DesiredQrCodeSize As Integer, ByVal CsvInput As String,
                                        ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                        ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreatePprV2PngCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprV2PngCsvBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String, ByVal Resolution As Integer,
                                                ByVal BoundaryLines As String, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean, ByVal AdditionalPrintMargin As Boolean,
                                                ByVal CsvInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreatePprPngJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprV1PngJsonBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String, ByVal Resolution As Integer,
                                                ByVal BoundaryLines As Boolean, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean,
                                                ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreatePprV2PngJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprV2PngJsonBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String, ByVal Resolution As Integer,
                                                ByVal BoundaryLines As String, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean, ByVal AdditionalPrintMargin As Boolean,
                                                ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function


    <DllImportAttribute(DLL_PATH, EntryPoint:="CreatePprV2PdfJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprV2PdfJsonBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String,
                                                ByVal BoundaryLines As String, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean, ByVal AdditionalPrintMargin As Boolean,
                                                ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreateSpcCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateSpcCsvBase64(ByVal CsvInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="CreateSpcJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateSpcJsonBase64(ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateJsonBase64(ByVal JsonInput As String,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCsvBase64(ByVal CsvInput As String,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function


    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateStringBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateStringBase64(ByVal Str As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateIBAN",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateIBAN(ByVal IBAN As String, ByVal ValidateCountryCode As Boolean) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateQrIBAN",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateQrIBAN(ByVal IBAN As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateQrReference",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateQrReference(ByVal QrReference As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateCreditorReference",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCreditorReference(ByVal CreditorReference As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute(DLL_PATH, EntryPoint:="ValidateCountryCode",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCountryCode(ByVal CountryCode As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

End Class

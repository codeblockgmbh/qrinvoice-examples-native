Imports System.Text
Imports System.IO

Module Program
    Sub Main(args As String())
        QrInvoice.SetFontPath(QrInvoice.QR_INVOICE_DIR)
        Console.WriteLine("-----------")
        Validate()
        Console.WriteLine("-----------")
        CreateSpc()
        Console.WriteLine("-----------")
        CreateQrCode()
        Console.WriteLine("-----------")
        CreatePaymentPartReceiptPngV1()
        Console.WriteLine("-----------")
        CreatePaymentPartReceiptPngV2()
        Console.WriteLine("-----------")
        CreatePaymentPartReceiptPngV2Csv()
        Console.WriteLine("-----------")
        CreatePaymentPartReceiptPdf()
        Console.WriteLine("-----------")
    End Sub

    Const INPUT_DATA_CSV As String =
        """CdtrInf/IBAN"",""CdtrInf/Cdtr/AdrTp"",""CdtrInf/Cdtr/Name"",""CdtrInf/Cdtr/StrtNmOrAdrLine1"",""CdtrInf/Cdtr/BldgNbOrAdrLine2"",""CdtrInf/Cdtr/PstCd"",""CdtrInf/Cdtr/TwnNm"",""CdtrInf/Cdtr/Ctry"",""CcyAmt/Amt"",""CcyAmt/Ccy"",""UltmtDbtr/AdrTp"",""UltmtDbtr/Name"",""UltmtDbtr/StrtNmOrAdrLine1"",""UltmtDbtr/BldgNbOrAdrLine2"",""UltmtDbtr/PstCd"",""UltmtDbtr/TwnNm"",""UltmtDbtr/Ctry"",""RmtInf/Tp"",""RmtInf/Ref"",""RmtInf/AddInf/Ustrd"",""RmtInf/AddInf/StrdBkgInf"",""AltPmtInf/AltPmt1"",""AltPmtInf/AltPmt2""" & vbCrLf _
        & """CH4431999123000889012"",""S"",""Robert Schneider AG"",""Rue du Lac"",""1268"",""2501"",""Biel"",""CH"",""0.7"",""CHF"",""S"",""Pia-Maria Rutschmann-Schnyder"",""Grosse Marktgasse"",""28"",""9400"",""Rohrschach"",""CH"",""QRR"",""210000000003139471430009017"",""Instruction of 03.04.2019"","""",,"

    Const INPUT_DATA_JSON As String = "{" &
        "  ""creditorInformation"":{" &
        "    ""iban"":""CH4431999123000889012""," &
        "    ""creditor"":{" &
        "      ""addressType"":""STRUCTURED""," &
        "      ""name"":""Robert Schneider AG""," &
        "      ""streetName"":""Rue du Lac""," &
        "      ""houseNumber"":""1268""," &
        "      ""postalCode"":""2501""," &
        "      ""city"":""Biel""," &
        "      ""country"":""CH""" &
        "    }" &
        "  }," &
        "  ""paymentAmountInformation"":{" &
        "    ""currency"":""CHF""," &
        "    ""amount"":1949.75" &
        "  }," &
        "  ""ultimateDebtor"":{" &
        "    ""addressType"":""STRUCTURED""," &
        "    ""name"":""Pia-Maria Rutschmann-Schnyder""," &
        "    ""streetName"":""Grosse Marktgasse""," &
        "    ""houseNumber"":""28""," &
        "    ""postalCode"":""9400""," &
        "    ""city"":""Rorschach""," &
        "    ""country"":""CH""" &
        "  }," &
        "  ""paymentReference"":{" &
        "    ""referenceType"":""QRR""," &
        "    ""reference"":""210000000003139471430009017""," &
        "    ""additionalInformation"":{" &
        "      ""unstructuredMessage"":""Instruction of 03.04.2019""" &
        "    }" &
        "  }," &
        "  ""alternativeSchemes"":{" &
        "    ""alternativeSchemeParameters"":[" &
        "      ""Name AV1: UV;UltraPay005;12345""," &
        "      ""Name AV2: XY;XYService;54321""" &
        "    ]" &
        "  }" &
        "}"

    Sub Validate()
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.ValidateJsonBase64(StringToBase64(INPUT_DATA_JSON), MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Console.WriteLine("Validate OK")
        Else
            Console.WriteLine("Validate error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If

    End Sub

    Sub CreateSpc()
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.CreateSpcJsonBase64(StringToBase64(INPUT_DATA_JSON), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Console.WriteLine("Swiss Payments Code: ")
            Console.WriteLine(Base64ToString(OutputBuffer.ToString()))
        Else
            Console.WriteLine("CreateSpc error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If

    End Sub
    Sub CreateQrCode()
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity
        Dim retCode As Integer = QrInvoice.CreateQrCodePngJsonBase64(500, StringToBase64(INPUT_DATA_JSON), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            File.WriteAllBytes("swissqrcode.png", Base64ToToByte(OutputBuffer.ToString()))
            Console.WriteLine("swissqrcode.png written")
        Else
            Console.WriteLine("CreateQrCode error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If

    End Sub

    Sub CreatePaymentPartReceiptPngV1()
        Dim OutputBuffer As StringBuilder = New StringBuilder(150000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.CreatePprV1PngJsonBase64("DIN_LANG", "LiberationSans", "GERMAN", 150, False, False, False, StringToBase64(INPUT_DATA_JSON), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            File.WriteAllBytes("paymentpartreceipt_v1.png", Base64ToToByte(OutputBuffer.ToString()))
            Console.WriteLine("paymentpartreceipt_v1.png written")
        Else
            Console.WriteLine("CreatePaymentPartReceiptPngV1 error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If
    End Sub

    Sub CreatePaymentPartReceiptPngV2()
        Dim OutputBuffer As StringBuilder = New StringBuilder(150000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.CreatePprV2PngJsonBase64("DIN_LANG", "LiberationSans", "GERMAN", 150, "none", True, False, False, StringToBase64(INPUT_DATA_JSON), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            File.WriteAllBytes("paymentpartreceipt_v2.png", Base64ToToByte(OutputBuffer.ToString()))
            Console.WriteLine("paymentpartreceipt_v2.png written")
        Else
            Console.WriteLine("CreatePaymentPartReceiptPngV2 error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If
    End Sub

    Sub CreatePaymentPartReceiptPngV2Csv()
        Dim OutputBuffer As StringBuilder = New StringBuilder(150000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.CreatePprV2PngCsvBase64("DIN_LANG", "LiberationSans", "GERMAN", 150, "none", True, False, False, StringToBase64(INPUT_DATA_CSV), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            File.WriteAllBytes("paymentpartreceipt_csv_v2.png", Base64ToToByte(OutputBuffer.ToString()))
            Console.WriteLine("paymentpartreceipt_csv_v2.png written")
        Else
            Console.WriteLine("CreatePaymentPartReceiptPngV2 CSV error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If
    End Sub


    Sub CreatePaymentPartReceiptPdf()
        Dim OutputBuffer As StringBuilder = New StringBuilder(150000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = QrInvoice.CreatePprV2PdfJsonBase64("A4", "LiberationSans", "FRENCH", "none", True, True, False, StringToBase64(INPUT_DATA_JSON), OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            File.WriteAllBytes("paymentpartreceipt.pdf", Base64ToToByte(OutputBuffer.ToString()))
            Console.WriteLine("paymentpartreceipt.pdf written")
        Else
            Console.WriteLine("CreatePaymentPartReceiptPdf error: " & retCode)
            Console.WriteLine(Base64ToString(MessageBuffer.ToString()))
        End If
    End Sub


    Function StringToBase64(str As String) As String
        Dim data() As Byte = System.Text.Encoding.UTF8.GetBytes(str)
        Return System.Convert.ToBase64String(data)
    End Function

    Function Base64ToString(base64 As String) As String
        Dim data() As Byte = Base64ToToByte(base64)
        Return System.Text.Encoding.UTF8.GetString(data)
    End Function

    Function Base64ToToByte(base64 As String) As Byte()
        Return System.Convert.FromBase64String(base64)
    End Function
End Module
